from getBlogInfo import get_blog_info
from saveComment import save_comment

get_blog_info_round = 10  # 调整该参数能控制爬取微博文章数量

if __name__ == '__main__':
    blog_info_list = []
    for i in range(get_blog_info_round):
        get_blog_info(blog_info_list)
    print("爬取微博数量：{}\n{}".format(len(blog_info_list), blog_info_list))
    print("*" * 90)

    total_comment = 0
    image_text_comment = 0
    for i in range(len(blog_info_list)):
        user_id = blog_info_list[i][0]
        blog_id = blog_info_list[i][1]
        current_total_comment, current_image_text_comment = save_comment(user_id, blog_id)
        total_comment += current_total_comment
        image_text_comment += current_image_text_comment
        print("[{}/{}]\nuser_id = {}\nblog_id = {}\ncurrent_total_comment = {}\ncurrent_image_text_comment = {}\ntotal_comment = {}\nimage_text_comment = {}".format(i + 1, len(blog_info_list), user_id, blog_id, current_total_comment, current_image_text_comment, total_comment, image_text_comment))
        print("=" * 90)
    print("total_comment = {}\nimage_text_comment = {}".format(total_comment, image_text_comment))