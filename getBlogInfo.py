import string

import requests


# 返回微博user_id和blog_id的集合
def get_blog_info(blog_info_list):
    url = "https://weibo.com/ajax/feed/unreadfriendstimeline"
    headers = {
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) "
                      "Chrome/108.0.0.0 Safari/537.36 Edg/108.0.1462.46 ",
        "x-requested-with": "XMLHttpRequest",
        "cookie": "s_tentry=-; Apache=1410126710391.8213.1700878894996; SINAGLOBAL=1410126710391.8213.1700878894996; ULV=1700878895015:1:1:1:1410126710391.8213.1700878894996:; XSRF-TOKEN=f_2n3pIpNVWWJvH_WDs2LnzD; SUB=_2A25IZSofDeRhGeBH7FES8CvFzTWIHXVrGyPXrDV8PUNbmtANLW7WkW9NQac5_5hKZEngE8jVyAKYnq2UJ_jnvGU7; SUBP=0033WrSXqPxfM725Ws9jqgMF55529P9D9WhpirFfp_OIu5NcS6MZTSIq5JpX5KzhUgL.Foq4S0e0eh-4So.2dJLoI0qLxKqL1-BL12-LxK-LBo5L12qLxKML1hqL122LxKML1-BLBK2LxKBLBonL12BLxKML1-BL1h5t; ALF=1732414927; SSOLoginState=1700878927; WBPSESS=jmYOiI7q7Av0hh4yNb-xXra3JC84KG_ZrBgVqC8lwvpfUT_VCuvaMW4vkrPnHUfNo6sGYLyDAVFi5yp1jEuv5_2ukzN7FgoM5i2iGuiEUMyUVPZmkmS0cw4bW2h5hqgmEJOwXHVMtJFcK3X_ZDf4Yw==",
        "x-xsrf-token": "-YYOKoKzkyMDGhDmhVSCLqpD"
    }
    response = requests.get(url, headers=headers)
    statuses_list = response.json()["statuses"]

    for status in statuses_list:
        if status["comments_count"] == 0: break  # 过滤0评论微博文章
        user_id = str(status["user"]["id"])
        blog_id = status["mblogid"]
        blog_info_list.append([user_id, blog_id])
    return blog_info_list
