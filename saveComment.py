import time
import csv
import requests

total_comment = 0
image_text_comment = 0


def getArticleId(comment_id, headers):
    """
    :param comment_id: 需要解密的id字符串
    :return:
    """
    url_id = "https://weibo.com/ajax/statuses/show?id={}".format(comment_id)
    resp_id = requests.get(url_id, headers=headers)
    article_id = resp_id.json()["id"]
    return article_id


def get_one_page(params, headers, writer):
    """
    :param params: get请求需要的参数，数据类型为字典
    :return: max_id：请求所需的另一个参数
    """
    url = "https://weibo.com/ajax/statuses/buildComments"
    resp = requests.get(url, headers=headers, params=params)
    data_list = resp.json()["data"]
    global total_comment, image_text_comment
    total_comment += len(data_list)  # 更新总评论数
    for data in data_list:
        data_dict = {
            "用户名": data["user"]["screen_name"],
            "用户头像": data["user"]["profile_image_url"],
            "IP地址": data["user"]["location"],
            "评论时间": data["created_at"].replace("+0800", ""),
            "评论内容": data["text_raw"],
            "点赞数量": data["like_counts"],
            "回复数量": data["total_number"],
        }
        if "http://t.cn/" in data["text_raw"]: image_text_comment += 1  # 更新图文评论数
        saveData(data_dict, writer)
    max_id = resp.json()["max_id"]
    if max_id:
        return max_id
    else:
        return


# 微博评论有懒加载机制，每次只会加载一页，使用get_all_data函数会爬取本条微博的所有评论
def get_all_data(params, headers, writer):
    """
    :param params: get请求需要的参数，数据类型为字典
    :return:
    """
    max_id = get_one_page(params, headers, writer)
    params["max_id"] = max_id
    params["count"] = 20
    while max_id:
        params["max_id"] = max_id
        time.sleep(.5)
        max_id = get_one_page(params)


def saveData(data_dict, writer):
    """
    :param data_dict: 要保存的数据，形式为dict类型
    :return:
    """
    writer.writerow(data_dict)


def save_comment(uid, comment_id):
    global total_comment, image_text_comment
    total_comment = 0
    image_text_comment = 0
    print("正在爬取 https://weibo.com/{}/{}".format(uid, comment_id))
    headers = {
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) "
                      "Chrome/108.0.0.0 Safari/537.36 Edg/108.0.1462.46 ",
        "x-requested-with": "XMLHttpRequest",
        "referer": "https://weibo.com/1779719003/{}".format(comment_id),
        "cookie": "s_tentry=-; Apache=1410126710391.8213.1700878894996; SINAGLOBAL=1410126710391.8213.1700878894996; ULV=1700878895015:1:1:1:1410126710391.8213.1700878894996:; XSRF-TOKEN=f_2n3pIpNVWWJvH_WDs2LnzD; SUB=_2A25IZSofDeRhGeBH7FES8CvFzTWIHXVrGyPXrDV8PUNbmtANLW7WkW9NQac5_5hKZEngE8jVyAKYnq2UJ_jnvGU7; SUBP=0033WrSXqPxfM725Ws9jqgMF55529P9D9WhpirFfp_OIu5NcS6MZTSIq5JpX5KzhUgL.Foq4S0e0eh-4So.2dJLoI0qLxKqL1-BL12-LxK-LBo5L12qLxKML1hqL122LxKML1-BLBK2LxKBLBonL12BLxKML1-BL1h5t; ALF=1732414927; SSOLoginState=1700878927; WBPSESS=jmYOiI7q7Av0hh4yNb-xXra3JC84KG_ZrBgVqC8lwvpfUT_VCuvaMW4vkrPnHUfNo6sGYLyDAVFi5yp1jEuv5_2ukzN7FgoM5i2iGuiEUMyUVPZmkmS0cw4bW2h5hqgmEJOwXHVMtJFcK3X_ZDf4Yw==",
        "x-xsrf-token": "-YYOKoKzkyMDGhDmhVSCLqpD"
    }
    id = getArticleId(comment_id, headers)  # 获取参数需要的真正id
    params = {  # get请求的参数
        "is_reload": 1,
        "id": id,
        "is_show_bulletin": 2,
        "is_mix": 0,
        "count": 10,
        "uid": int(uid)
    }

    # 向csv文件写入表头
    csv_name = uid + "_" + comment_id
    csv_header = ["用户名", "用户头像", "IP地址", "评论时间", "评论内容", "点赞数量", "回复数量"]
    f = open(f"./csv/{csv_name}.csv", "w", encoding="utf-8", newline="")
    writer = csv.DictWriter(f, csv_header)
    writer.writeheader()

    get_one_page(params, headers, writer)
    f.close()
    return total_comment, image_text_comment
