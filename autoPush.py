from datetime import datetime
import os
import subprocess

def run_git_commands():
    # 获取当前时间并格式化为"MM-DD HH:MM"的形式
    current_time = datetime.now().strftime("%m-%d %H:%M")

    # 获取当前文件夹路径
    current_directory = os.getcwd()

    # 检查是否有变更需要添加到Git仓库
    git_status = subprocess.run(["git", "status", "--porcelain"], capture_output=True, cwd=current_directory, text=True)

    if git_status.stdout.strip() == "":
        print("No changes to commit.")
    else:
        # 在当前文件夹中打开cmd窗口并执行带时间的git commit命令
        commit_message = f"{current_time}"
        subprocess.run(["git", "add", "*"], cwd=current_directory)
        subprocess.run(["git", "commit", "-m", commit_message], cwd=current_directory)
        subprocess.run(["git", "push"], cwd=current_directory)

if __name__ == "__main__":
    run_git_commands()
